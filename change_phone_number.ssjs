<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
<script runat="server">

     Platform.Load("core", "1.1.1");
     var name_of_DE_from="Phone_number11";
     var name_of_DE_to='Phone_number11';
     var name_of_column_of_phone='phone_number_before';
     var name_of_primary_key_from='email';
     var name_of_primary_key_to='email';
     var name_of_destination_column='phone_number_type';

            try {

                var data= retrieveAllRecords(name_of_DE_from);
                var updateDE=DataExtension.Init(name_of_DE_to);

                for(var i = 0; i < data.length; i++) {
                  var after=data[i][name_of_column_of_phone].replace(/\D/g,'');
                  data[i][name_of_primary_key]
                  updateDE.Rows.Update({phone_number_type:after}, [name_of_primary_key_to], [data[i][name_of_primary_key_from]]);
                  //change phone_number_type take value of name_of_destination_column and pass column

                }

            } catch (err) {
                Write("error!"+ err);

            }



        function retrieveFieldNames(name) {

          var attr = DataExtension.Retrieve({ Property: "Name", SimpleOperator: "equals", Value: name });

          var de = DataExtension.Init(attr[0].CustomerKey);

          var fields = de.Fields.Retrieve();

          fields.sort(function (a, b) { return (a.Ordinal > b.Ordinal) ? 1 : -1 });

          var out = [];

          for (k in fields) {
              out = out.concat(fields[k].Name);
          }

          return out;

      }

      function retrieveAllRecords(name) {

          var prox = new Script.Util.WSProxy();

          var cols = retrieveFieldNames(name);

          var config = {
              name: name,
              cols: cols
          }

          var records = [],
              moreData = true,
              reqID = data = null;

          while (moreData) {

              moreData = false;

              if (reqID == null) {
                  data = prox.retrieve("DataExtensionObject[" + config.name + "]", config.cols);
              } else {
                  data = prox.getNextBatch("DataExtensionObject[" + config.name + "]", reqID);
              }

              if (data != null) {
                  moreData = data.HasMoreRows;
                  reqID = data.RequestID;
                  for (var i = 0; i < data.Results.length; i++) {
                      var result_list = data.Results[i].Properties;
                      var obj = {};
                      for (k in result_list) {
                          var key = result_list[k].Name;
                          var val = result_list[k].Value
                          if (key.indexOf("_") != 0) obj[key] = val;
                      }
                      records.push(obj);
                  }
              }
          }
          return records;
      }
</script>
</body>
</html>
