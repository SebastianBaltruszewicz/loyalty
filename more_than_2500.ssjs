<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
<script runat="server">

Platform.Load("core", "1.1.1");

     //debug
      var debug1="debug"; // debug or info
      var level1="";
      var warning_level1=15; // level for warning
      var reportDEname="reportLoylaty"; //report data extansion
      var reportDE=DataExtension.Init(reportDEname);

      //sebastian update
      var nameOfDE='Orders_Loyalty'; //data extansion for sum up of orders
      var records = retrieveAllRecords(nameOfDE);
      var filterUsers1=[];
      for(var i = 0; i < records.length; i++) {
        if(parseFloat(records[i]["TotalSpend"])>100&&parseFloat(records[i]["TotalSpend"])<106){
          filterUsers1.push(records[i]);
        }

      }
      Write("This is filterUsers1: ");
      Write(filterUsers1.length+"</br>");
      var nameOfCodesDE='Loyalty_Coupons';
      var codesDE = DataExtension.Init('Loyalty_Coupons');
      var records_Coupons = retrieveAllRecords(nameOfCodesDE);
      var assignCoupons=[];
      var notAssignCoupons=[];
      for(var i = 0; i < records_Coupons.length; i++) {
        if(records_Coupons[i]["Assigned"]=="empty"){
          notAssignCoupons.push(records_Coupons[i]);
        }else{
          assignCoupons.push(records_Coupons[i])
        }

      }
      Write("This is not assign: ");
      Write(notAssignCoupons.length+"</br>");
      Write("This is assign: ");
      Write(assignCoupons.length+"</br>");

      coupons(filterUsers1,notAssignCoupons,assignCoupons,debug1,warning_level1,codesDE);

      Write("Done");


     function coupons(filterUsers,notUsecodesDE,useCodesDE,debug,warning_level,codesDE){
       debugLog(debug,"info","start of script");
       if(notUsecodesDE.length>filterUsers.length){
         if(notUsecodesDE.length<warning_level){
           debugLog(debug,"warn","Warning coupons level less then: "+warning_level+" number of available coupons: "+notUsecodesDE.length);
         }
        var j=0;
        for(var i = 0; i < filterUsers.length; i++) {
          if(isEmailNotUsed(filterUsers[i]["EmailAddress"], useCodesDE)){
            codesDE.Rows.Update({Assigned: filterUsers[i]["EmailAddress"], send_to_consumer:false, date_of_assign_coupons: new Date()}, ["Promo_Code"], [notUsecodesDE[j]["Promo_Code"]] );
            debugLog(debug,"info","voucher added for: "+filterUsers[i]["EmailAddress"])
            j++;
          }
       }
       }else{
        debugLog(debug,"error","not enough coupons !!")
      }
       debugLog(debug,"info","end of script")
     }

      function isEmailNotUsed(emailAssign, tableofUsedCoupons) {
      var result=true;
       for(var i=0;i<tableofUsedCoupons.length;i++){
           if(emailAssign==tableofUsedCoupons[i]["Assigned"]){
                result=false;
                Write(emailAssign+"has already coupon </br>");
            }

         }
         return result;
      }

      function debugLog(status, level,message){
        if(status=="debug"){
           if(level=="error"){
             var new_date= Format(Now(),"MM/dd/yyyy HH:MM:ss.mm");
            reportDE.Rows.Add({log:"multifilter: "+message, date: new_date, status: "ERROR"});
         }
          if(level=="info"){
            var new_date= Format(Now(),"MM/dd/yyyy HH:MM:ss");
            reportDE.Rows.Add({log:"multifilter: "+message, date: new_date, status: "info"});
         }
          if(level=="warn"){
            var new_date= Format(Now(),"MM/dd/yyyy HH:MM:ss.mm");
            reportDE.Rows.Add({log:"multifilter: "+message, date: new_date, status: "WARNING"});
         }
        }
        if(status=="info"){
           if(level=="error"){
             var new_date= Format(Now(),"MM/dd/yyyy HH:MM:ss.mm");
            reportDE.Rows.Add({log:"multifilter: "+message, date: new_date, status: "ERROR"});
         }
           if(level=="warn"){
             var new_date= Format(Now(),"MM/dd/yyyy HH:MM:ss.mm");
            reportDE.Rows.Add({log:"multifilter: "+message, date: new_date, status: "WARNING"});
         }
        }


      }


      function retrieveFieldNames(name) {

          var attr = DataExtension.Retrieve({ Property: "Name", SimpleOperator: "equals", Value: name });

          var de = DataExtension.Init(attr[0].CustomerKey);

          var fields = de.Fields.Retrieve();

          fields.sort(function (a, b) { return (a.Ordinal > b.Ordinal) ? 1 : -1 });

          var out = [];

          for (k in fields) {
              out = out.concat(fields[k].Name);
          }

          return out;

      }

      function retrieveAllRecords(name) {

          var prox = new Script.Util.WSProxy();

          var cols = retrieveFieldNames(name);

          var config = {
              name: name,
              cols: cols
          }

          var records = [],
              moreData = true,
              reqID = data = null;

          while (moreData) {

              moreData = false;

              if (reqID == null) {
                  data = prox.retrieve("DataExtensionObject[" + config.name + "]", config.cols);
              } else {
                  data = prox.getNextBatch("DataExtensionObject[" + config.name + "]", reqID);
              }

              if (data != null) {
                  moreData = data.HasMoreRows;
                  reqID = data.RequestID;
                  for (var i = 0; i < data.Results.length; i++) {
                      var result_list = data.Results[i].Properties;
                      var obj = {};
                      for (k in result_list) {
                          var key = result_list[k].Name;
                          var val = result_list[k].Value
                          if (key.indexOf("_") != 0) obj[key] = val;
                      }
                      records.push(obj);
                  }
              }
          }
          return records;
      }
</script>
</body>
</html>
