<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <script runat="server">
      Platform.Load("Core","1.1.1");
      //debug
      var debug1="debug"; // debug or info
      var level1="";
      var warning_level1=15; // level for warning
      var reportDEname="reportLoylaty"; //report data extansion
      var reportDE=DataExtension.Init(reportDEname);

      //sebastian update
      var nameOfDE='Orders_Loyalty'; //data extansion for sum up of orders
      var loylatyOrdersDE = DataExtension.Init(nameOfDE);
      var filter1 = {Property:"TotalSpend",SimpleOperator:"greaterThanOrEqual",Value: "100"};
      var filterUsers1 = loylatyOrdersDE.Rows.Retrieve(filter1);

      //data extansion with codes
      var nameOfCodesDE='Loyalty_Coupons'; //tutaj wpisz nazwe data extansion z kuponami
      var codesDE = DataExtension.Init(nameOfCodesDE);
      var filter2 = {Property:"Assigned", SimpleOperator:"equals",Value: "empty"}; //check if coupons is not used
      var filter3 = {Property:"Assigned", SimpleOperator:"NotEquals",Value: "empty"};//used coupons
      var notUsecodesDE1 = codesDE.Rows.Retrieve(filter2);
      var useCodesDE1 = codesDE.Rows.Retrieve(filter3);

      //let's assign coudes
      assignCoupons(filterUsers1, notUsecodesDE1, debug1, warning_level1, useCodesDE1);

      function assignCoupons(filterUsers, notUsecodesDE, debug, warning_level, useCodesDE){
        debugLog(debug,"info","start of script")
      if(notUsecodesDE.length>filterUsers.length){
        if(notUsecodesDE.length<warning_level){
           debugLog(debug,"warn","Warning coupons level less then: "+warning_level+" number of available coupons: "+notUsecodesDE.length);
           }
        var j=0;
        for(var i = 0; i < filterUsers.length; i++) {

          if(isEmailNotUsed(filterUsers[i].EmailAddress, useCodesDE)){
            codesDE.Rows.Update({Assigned: filterUsers[i].EmailAddress, send_to_consumer:false, date_of_assign_coupons: new Date()}, ["Promo_Code"], [notUsecodesDE[j].Promo_Code] );
            debugLog(debug,"info","voucher added for: "+filterUsers[i].EmailAddress)
            j++;
          }
       }
      }else{
        debugLog(debug,"error","not enough coupons !!")
      }

      debugLog(debug,"info","end of script")

      debugLog(debug,"info","end of script")

      }




      function debugLog(status, level,message){
        if(status=="debug"){
           if(level=="error"){
             var new_date= Format(Now(),"MM/dd/yyyy HH:MM:ss.mm");
            reportDE.Rows.Add({log:"multifilter: "+message, date: new_date, status: "ERROR"});
         }
          if(level=="info"){
            var new_date= Format(Now(),"MM/dd/yyyy HH:MM:ss");
            reportDE.Rows.Add({log:"multifilter: "+message, date: new_date, status: "info"});
         }
          if(level=="warn"){
            var new_date= Format(Now(),"MM/dd/yyyy HH:MM:ss.mm");
            reportDE.Rows.Add({log:"multifilter: "+message, date: new_date, status: "WARNING"});
         }
        }
        if(status=="info"){
           if(level=="error"){
             var new_date= Format(Now(),"MM/dd/yyyy HH:MM:ss.mm");
            reportDE.Rows.Add({log:"multifilter: "+message, date: new_date, status: "ERROR"});
         }
           if(level=="warn"){
             var new_date= Format(Now(),"MM/dd/yyyy HH:MM:ss.mm");
            reportDE.Rows.Add({log:"multifilter: "+message, date: new_date, status: "WARNING"});
         }
        }


      }

      function isEmailNotUsed(emailAssign, tableofUsedCoupons) {
      var result=true;
       for(var i=0;i<tableofUsedCoupons.length;i++){
           if(emailAssign==tableofUsedCoupons[i]["Assigned"]){
                result=false;
            }

         }
         return result;
      }

    }
  </script>
  </body>
</html>
