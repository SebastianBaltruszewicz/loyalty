<!doctype html>
<html>
<head>
 <meta charset="utf-8">
 <title></title>
 <meta name="description" content="">
 <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <script runat="server">
     Platform.Load("Core", "1.1.5");
        changeFileNameFTP('"'+changeFileName("/Export/TestFileRename/rb_amer_child.dat")+'"', '"'+changeFileName("old_rb_amer_child_ddmmyyyy.dat")+'"','"6364438"');

        //Write(Stringify(changeFileName("/Export/TestFileRename/rb_amer_child_ddyyyymm120009.dat") + ''));
        function changeFileName(text) {
          var mydate = new Date();
          if(mydate.getDate() < 10) text = text.replace('dd', '0' + mydate.getDate());
          else text = text.replace('dd', mydate.getDate());
          if((mydate.getMonth()+1) < 10) text = text.replace('mm', '0' + (mydate.getMonth()+1));
          else text = text.replace('mm', (mydate.getMonth()+1));
          text = text.replace('yyyy', mydate.getFullYear());
          return text;
        }
        function changeFileNameFTP(filePath, newFileName, MIDname) {
          try {
              var url = 'http://sfmc-ftp-management-api.us-w1.cloudhub.io';
              var contentType = 'application/json';
              var headerNames = ["authorization"];
              var headerValues = ["Basic c2ZtY0FwaVJCOk11bGVAdENEZXYhMjM="];
              var payload = '{ "oldFilePath":' + filePath + ', "newFileName":' + newFileName + ', "MID":' + MIDname + '}';
              //Write(Stringify(payload) + '<br>');
              var result = HTTP.Post(url, contentType, payload, headerNames, headerValues);
              var resultCode = (result);
              var obj = Platform.Function.ParseJSON(result.Response + '');
              Write(Stringify(obj) + '<br>');
            } catch (ex) {
              Write("error message: " + ex);
              Write("<br> Error name: "+Stringify(ex.name));
              Write("<br> Error message: "+Stringify(ex.message));
          }
        }
  </script>
</body>
</html>
